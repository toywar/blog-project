from __future__ import with_statement
from fabric.api import local, settings, abort
from fabric.contrib.console import confirm

'''
def prepare_deploy():
	local("../../manage.py test blog")
	local("git add . && git commit -m 'add fabfile' ")
	local("git push")
'''

def test_app():
	with settings(warn_only = True):
		result = local("../../manage.py test blog", capture = True)
	if result.failed and not confirm("Test failed! Continue anyway?"):
	 	abort("Aborting at user request!")

def commit():
	local("git add . && git commit -m 'update fabfile' ")

def push():
	local("git push")

def prepare_deploy():
	test_app()
	commit()
	push()
